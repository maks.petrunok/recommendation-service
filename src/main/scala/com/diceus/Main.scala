package com.diceus

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import com.diceus.config.AppConfig
import com.diceus.routes.GetProductsRoute
import com.diceus.service.RecommendationDao
import com.sksamuel.elastic4s.http.JavaClient
import com.sksamuel.elastic4s.{ElasticClient, ElasticProperties}
import com.typesafe.scalalogging.LazyLogging
import pureconfig.ConfigSource
import pureconfig.generic.auto._
import redis.RedisClient
import java.util.concurrent.atomic.AtomicReference
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._

object Main extends App {

  private val config = ConfigSource.default.loadOrThrow[AppConfig]

  new Main(config).startServer()
}

class Main(config: AppConfig) extends LazyLogging {

  private implicit val system: ActorSystem = ActorSystem()
  private implicit val dbExecutionContext: ExecutionContext =
    system.dispatchers.lookup("akka.contexts.blocking-dispatcher")

  val props: ElasticProperties = ElasticProperties(
    config.elasticClient.hosts.mkString(",")
  )
  val elasticClient: ElasticClient = ElasticClient(JavaClient(props))
  val redisClient: RedisClient = RedisClient()

  private val recommendationDao =
    new RecommendationDao(elasticClient, redisClient, config.recommendation)(
      dbExecutionContext
    )

  private val getProductsRoute = new GetProductsRoute(recommendationDao).routes

  private val server: AtomicReference[Option[Future[Http.ServerBinding]]] =
    new AtomicReference(None)

  def startServer(): Unit = {
    val startedServer = server.compareAndSet(
      None,
      Some(
        Http()
          .newServerAt(config.http.host, config.http.port)
          .bind(getProductsRoute)
      )
    )

    if (startedServer) {
      logger.info(s"Started server on ${config.http.host}:${config.http.port}")
    } else {
      logger.warn("Attempt to start server more than once")
    }
  }

  def stopServer(): Unit = {
    server.getAndSet(None) match {
      case Some(bindingFuture) =>
        val serverTerminationF = Await
          .result(bindingFuture, 5.seconds)
          .terminate(hardDeadline = config.shutdownTimeout)
          .map(_ => system.terminate())(ExecutionContext.global)
        Await.result(serverTerminationF, Duration.Inf)

      case None =>
        logger.warn("No server binding to terminate")
    }
    redisClient.stop()
  }

  Runtime.getRuntime.addShutdownHook(new Thread {
    override def run: Unit = {
      logger.info("Shutting down...")
      stopServer()
      logger.info("Shutdown complete")
    }
  })
}
