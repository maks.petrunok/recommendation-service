package com.diceus.types

import cats.data.EitherT
import io.circe.generic.JsonCodec

import scala.concurrent.Future

object Types {
  type ErrorOr[T] = Either[String, T]
  type ErrorOrF[T] = EitherT[Future, String, T]

  @JsonCodec
  case class Product(
      item_id: String,
      name: String,
      locale: String,
      click: Int,
      purchase: Int
  )
}
