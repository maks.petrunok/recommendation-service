package com.diceus.types

import enumeratum.values.{IntEnum, IntEnumEntry}

sealed abstract class RecommendationConfigId(val value: Int, val key: String)
    extends IntEnumEntry

object RecommendationConfigId extends IntEnum[RecommendationConfigId] {

  val values = findValues

  case object ClickBased extends RecommendationConfigId(1, "click")
  case object PurchaseBased extends RecommendationConfigId(2, "purchase")
}
