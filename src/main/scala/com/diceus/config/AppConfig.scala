package com.diceus.config

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration._

case class AppConfig(
    http: HttpConfig,
    elasticClient: ElasticSearchClientConfig,
    recommendation: RecommendationConfig,
    shutdownTimeout: FiniteDuration = 10.seconds
)

case class HttpConfig(
    host: String,
    port: Int
)

case class ElasticSearchClientConfig(
    hosts: Seq[String]
)

case class RecommendationConfig(
    productsIndex: String
)
