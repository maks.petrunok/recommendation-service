package com.diceus.service

import cats.data.EitherT
import com.diceus.service.RecommendationDao._
import com.diceus.types.RecommendationConfigId
import com.diceus.types.Types._
import com.sksamuel.elastic4s._
import com.sksamuel.elastic4s.ElasticDsl._
import scala.concurrent.Future
import io.circe.generic.JsonCodec
import io.circe.parser._
import redis.RedisClient
import scala.concurrent.ExecutionContext
import scala.util.Try
import cats.implicits._
import com.diceus.config.RecommendationConfig
import io.circe.generic.auto._

class RecommendationDao(
    elasticClient: ElasticClient,
    redisClient: RedisClient,
    config: RecommendationConfig
)(implicit
    ec: ExecutionContext
) {

  private def elasticQuery(
      aggregationKey: String,
      size: Int,
      fromItem: Int
  ): ErrorOrF[Seq[Product]] =
    EitherT {
      elasticClient
        .execute {
          search(config.productsIndex)
            .query(matchAllQuery())
            .sortByFieldDesc(aggregationKey)
            .from(fromItem)
            .size(size)
        }
        .map {
          case RequestSuccess(status, _, _, result) if status == ResponseOk =>
            result.hits.hits.toList
              .traverse { hit =>
                decode[Product](hit.sourceAsString)
              }
              .leftMap(_.getLocalizedMessage)

          case RequestSuccess(status, _, _, result) =>
            Left(s"Unexpected search result [$status]: ${result.toString}")

          case RequestFailure(status, _, _, error) =>
            Left(s"Unexpected error [$status]: ${error}")
        }
    }

  def getProducts(
      pageSize: Int,
      pageNum: Int,
      configId: Option[RecommendationConfigId]
  ): ErrorOrF[Seq[Product]] = {

    val from: Int = pageSize * pageNum

    val configIdFuture: ErrorOrF[RecommendationConfigId] =
      if (configId.nonEmpty)
        EitherT.right(Future.successful(configId.get))
      else getRecommendationConfigId

    for {
      configId <- configIdFuture
      result <- elasticQuery(configId.key, pageSize, from)
    } yield result
  }

  private def getRecommendationConfigId: ErrorOrF[RecommendationConfigId] =
    EitherT(
      redisClient
        .get(RecommendationConfigKey)
        .map {
          case Some(bs) =>
            Try(bs.utf8String.toInt).toEither
              .flatMap(RecommendationConfigId.withValueEither)
              .leftMap(_.getLocalizedMessage)

          case None =>
            Left("unable to fetch remote configId")
        }
    )
}

object RecommendationDao {

  final val RecommendationConfigKey = "recommendation:config_id"

  final val ResponseOk = 200

  final val ItemId: String = "name.keyword"
  final val AggregationKey: String = "points"

  @JsonCodec
  case class ResultItem(key: String)

  @JsonCodec
  case class ProductsResultSet(buckets: Seq[ResultItem]) {
    def getNames: Seq[String] = buckets.map(_.key)
  }

}
