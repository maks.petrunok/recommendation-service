package com.diceus.routes

import akka.http.scaladsl.model._
import akka.http.scaladsl.server.{Directive0, Route}
import akka.http.scaladsl.server.Directives._
import com.diceus.service.RecommendationDao
import com.diceus.types.{RecommendationConfigId, Types}
import com.diceus.types.Types.ErrorOr
import com.typesafe.scalalogging.LazyLogging
import io.circe.Encoder
import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller}
import scala.concurrent.Future
import scala.util.{Failure, Success}
import io.circe.syntax._
import io.circe.generic.auto._

class GetProductsRoute(recommendationDao: RecommendationDao)
    extends LazyLogging {

  implicit final def marshaller[A: Encoder]: ToEntityMarshaller[A] = {
    Marshaller.withFixedContentType(ContentTypes.`application/json`) { a =>
      HttpEntity(ContentTypes.`application/json`, a.asJson.noSpaces)
    }
  }

  val routes: Route =
    path("products") {
      (get & parameters(
        "size".as[Int],
        "page".as[Int],
        "configId".as[Int].optional
      )) { (size, page, configId) =>
        (validateSize(size) & validatePage(page) & validateConfigId(configId)) {
          val recommendationConfigId =
            configId.map(RecommendationConfigId.withValue)

          val elasticSearchResponse: Future[ErrorOr[Seq[Types.Product]]] =
            recommendationDao
              .getProducts(size, page, recommendationConfigId)
              .value
          validateOnComplete(elasticSearchResponse)
        }
      }
    }

  private def validateSize(size: Int): Directive0 =
    validate(size >= 1 && size <= 10000, "size should be between 1 and 10000")

  private def validatePage(page: Int): Directive0 =
    validate(page >= 0, "page cannot be negative number")

  private def validateConfigId(configId: Option[Int]): Directive0 =
    validate(
      configId.isEmpty || configId
        .flatMap(RecommendationConfigId.withValueOpt)
        .nonEmpty,
      s"unknown configId value [${configId.mkString}]"
    )

  private def validateOnComplete[A: ToEntityMarshaller](
      future: Future[ErrorOr[A]]
  ): Route =
    onComplete[ErrorOr[A]](future) {
      case Success(Right(value)) =>
        complete(value)

      case Success(Left(value)) =>
        logger.error(s"Invalid result: $value")
        complete(StatusCodes.InternalServerError, "Unexpected error")

      case Failure(exception) =>
        logger.error("Unexpected error", exception)
        complete(StatusCodes.InternalServerError, "Unexpected error")
    }
}
