package com.diceus

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.BeforeAndAfterAll
import akka.actor.ActorSystem
import com.diceus.config.{AppConfig, RecommendationConfig}
import com.diceus.service.RecommendationDao
import com.diceus.service.RecommendationDao.RecommendationConfigKey
import com.diceus.types.RecommendationConfigId
import com.diceus.types.Types.Product
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.http.JavaClient
import com.sksamuel.elastic4s.requests.common.RefreshPolicy
import com.sksamuel.elastic4s.{ElasticClient, ElasticProperties}
import pureconfig.ConfigSource
import pureconfig.generic.auto._
import redis.RedisClient
import java.util.UUID
import scala.concurrent.ExecutionContext
import scala.util.Random

class RecommendationDaoSuite extends AnyFunSuite with BeforeAndAfterAll {

  implicit val actorSystem = ActorSystem("test-sys")

  private val index = s"test-index-${UUID.randomUUID.toString}"

  private val config = ConfigSource.default
    .loadOrThrow[AppConfig]
    .copy(
      recommendation = RecommendationConfig(index)
    )

  private val props: ElasticProperties = ElasticProperties(
    config.elasticClient.hosts.mkString(",")
  )
  private val elasticClient: ElasticClient = ElasticClient(JavaClient(props))
  private val redisClient: RedisClient = RedisClient()

  private val recommendationDao =
    new RecommendationDao(elasticClient, redisClient, config.recommendation)(
      ExecutionContext.global
    )

  private val productsCount: Int = 3

  private val products: Seq[Product] = List.fill(productsCount)(
    Product(
      UUID.randomUUID.toString,
      s"name-${UUID.randomUUID.toString}",
      "en",
      Random.nextInt(1000),
      Random.nextInt(1000)
    )
  )

  override def beforeAll() = {
    elasticClient.execute {
      createIndex(index)
    }.await

    elasticClient.execute {
      bulk(
        products.map { product =>
          indexInto(index).fields(productToMap(product))
        }
      ).refresh(RefreshPolicy.Immediate)
    }.await
  }

  override def afterAll(): Unit = {
    elasticClient.execute {
      deleteIndex(index)
    }.await
  }

  test("should fetch sorted by click - provided by request param") {
    val fetchedProducts =
      recommendationDao
        .getProducts(productsCount, 0, Some(RecommendationConfigId.ClickBased))
        .value
        .await

    val expectedResult = products.sortBy(_.click)(Ordering[Int].reverse)

    assert(fetchedProducts.isRight, s"got left: $fetchedProducts")
    assert(fetchedProducts.right.get == expectedResult)

  }

  test("should fetch sorted by purchase - provided by request param") {
    val fetchedProducts =
      recommendationDao
        .getProducts(
          productsCount,
          0,
          Some(RecommendationConfigId.PurchaseBased)
        )
        .value
        .await

    val expectedResult = products.sortBy(_.purchase)(Ordering[Int].reverse)

    assert(fetchedProducts.isRight, s"got left: $fetchedProducts")
    assert(fetchedProducts.right.get == expectedResult)
  }

  test("should fetch sorted by click - fetched from storage") {

    setConfigId(RecommendationConfigId.ClickBased)

    val fetchedProducts =
      recommendationDao
        .getProducts(productsCount, 0, None)
        .value
        .await

    val expectedResult = products.sortBy(_.click)(Ordering[Int].reverse)

    assert(fetchedProducts.isRight, s"got left: $fetchedProducts")
    assert(fetchedProducts.right.get == expectedResult)
  }

  test("should fetch sorted by purchase - fetched from storage") {

    setConfigId(RecommendationConfigId.PurchaseBased)

    val fetchedProducts =
      recommendationDao
        .getProducts(productsCount, 0, None)
        .value
        .await

    val expectedResult = products.sortBy(_.purchase)(Ordering[Int].reverse)

    assert(fetchedProducts.isRight, s"got left: $fetchedProducts")
    assert(fetchedProducts.right.get == expectedResult)
  }

  private def setConfigId(id: RecommendationConfigId): Unit = {
    val setResult: Boolean =
      redisClient
        .set(
          RecommendationConfigKey,
          id.value.toString
        )
        .await
    assert(setResult, s"Failed to set configId to ${id}")
  }

  private def productToMap(product: Product): Map[String, Any] =
    Map(
      "item_id" -> product.item_id,
      "name" -> product.name,
      "locale" -> product.locale,
      "click" -> product.click,
      "purchase" -> product.purchase
    )
}
