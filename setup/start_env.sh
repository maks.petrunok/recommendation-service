#!/bin/sh

docker run -d \
  -p 6379:6379 \
  --name=redis \
  redis

docker run -d \
  -p 9200:9200 \
  -p 9300:9300 \
  -e "discovery.type=single-node" \
  --name=elastic \
  docker.elastic.co/elasticsearch/elasticsearch:7.12.0
