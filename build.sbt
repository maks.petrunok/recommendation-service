name := "diceus"

version := "0.1"

scalaVersion := "2.13.0"

val AkkaVersion = "2.6.8"

libraryDependencies ++= Seq(
  "com.beachape" %% "enumeratum" % "1.6.1",
  "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http" % "10.2.4",
  "io.circe" %% "circe-parser" % "0.14.0-M5",
  "io.circe" %% "circe-generic" % "0.14.0-M5",
  "com.github.pureconfig" %% "pureconfig" % "0.15.0",
  "com.sksamuel.elastic4s" %% "elastic4s-client-esjava" % "7.12.0",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.3",
  "org.slf4j" % "slf4j-log4j12" % "1.7.30",
  "com.github.etaty" %% "rediscala" % "1.9.0",
  "org.typelevel" %% "cats-core" % "2.6.0",
  "org.scalatest" %% "scalatest" % "3.2.7" % "test"
)

scalacOptions ++= Seq(
  "-Ymacro-annotations"
)
